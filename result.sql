-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: sql307.byetcluster.com
-- Generation Time: May 04, 2023 at 01:40 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epiz_32509075_omega`
--

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `Id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `roll no` int(11) NOT NULL,
  `course` varchar(50) NOT NULL,
  `sem` int(11) NOT NULL,
  `city` varchar(100) NOT NULL,
  `total marks` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`Id`, `name`, `roll no`, `course`, `sem`, `city`, `total marks`) VALUES
(3, 'Kshitij Khobaragade', 65203, 'BCA', 3, 'Nagpur', 498),
(4, 'Sahil Chinchkede ', 65204, 'BCA', 3, 'Delhi', 456),
(5, 'Sameer Siddiqui', 65205, 'BCA', 2, 'Saoner', 317),
(6, 'Fakhar Jaman', 65206, 'BCA', 2, 'Nagaland', 120),
(7, 'Farhan Akhtar ', 65207, 'BCA', 2, 'Noida ', 348),
(8, 'Mousam Singh', 65207, 'BCA', 2, 'Banglore ', 244),
(20, 'Kajal Bagde', 65210, 'BCA', 2, 'Manali', 245),
(22, 'Anand Ahuja', 65211, 'BCA', 2, 'Noida', 367),
(23, 'Kartik Agarwal', 65212, 'BCA', 2, 'Nagpur', 234),
(24, 'Arohai Agarwal', 65213, 'BCA', 2, ' Film city ,Mumbai ', 201),
(25, 'Nikhil Reddy', 65214, 'BCA', 2, 'Hyderabad', 499),
(26, 'Harsh Patel', 65215, 'BCA', 2, 'Surat', 267),
(27, 'Charls Anthony', 65216, 'BCA', 2, 'Pune', 345),
(28, 'Sima Bakshi', 65217, 'BCA', 2, 'Jaipur', 126),
(29, 'Vijay Babu', 65218, 'BCA', 2, 'Lucknow', 399),
(30, 'Arya Banerjee', 65219, 'BCA', 2, 'Kanpur', 401),
(31, 'Pari Bhatt', 65220, 'BCA', 2, 'Indore', 376),
(32, 'Dev Basu', 65221, 'BCA', 2, 'Thane', 127),
(33, 'Shiva Varma', 65222, 'BCA', 2, 'Bhopal', 421),
(34, 'Om Chowdhury', 65223, 'BCA', 2, 'Visakhapatnam', 234),
(35, 'Riya Chakrabarti', 65224, 'BCA', 2, 'Pimpri & Chinchwad', 356),
(36, 'Maan Singh Chadha', 65225, 'BCA', 2, 'Patna', 134),
(37, 'Shabana Amin', 65226, 'BCA', 2, 'Vadodara', 189),
(38, 'Babu Apte', 65227, 'BCA', 2, 'Ghaziabad', 222),
(39, 'Dipak Malhotra', 65228, 'BCA', 2, 'Ludhiana', 115),
(40, 'Kiran Jha', 65229, 'BCA', 2, 'Agra', 342),
(41, 'Virat Joshi', 65230, 'BCA', 2, 'Nashik', 367),
(42, 'Bhagyashri Kohli', 65231, 'BCA', 2, 'Faridabad', 250),
(43, 'Krishnan Iyer', 65232, 'BCA', 2, 'Meerut', 175),
(44, ' Raj Khanna', 65233, 'BCA', 2, 'Rajkot', 367),
(45, 'Antara Gupta', 65234, 'BCA', 2, 'Kalyan & Dombivali', 333),
(46, 'Ajra Shekh', 65235, 'BCA', 2, 'Vasai Virar', 214),
(47, 'Sahil Kapoor', 65236, 'BCA', 2, 'Varanasi', 217),
(48, ' Chandra Mehta', 65237, 'BCA', 2, 'Srinagar', 423),
(49, 'Shyaama Mukherjee', 65238, 'BCA', 2, 'Aurangabad', 127),
(50, 'Khalil Shaha', 65239, 'BCA', 2, 'Dhanbad', 364),
(51, 'Sunny Sharma', 65240, 'BCA', 2, 'Haora', 100),
(60, 'Faisal  Ansari', 6512, 'BCA', 3, 'Nagpur', 456);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
